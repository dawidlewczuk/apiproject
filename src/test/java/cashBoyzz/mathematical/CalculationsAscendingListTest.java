package cashBoyzz.mathematical;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CalculationsAscendingListTest {

    @Test
    public void minValueTest() throws Exception {
        CalculationsAscendingList calculationsAscendingList = new CalculationsAscendingList();
        List<JsonTestData> jst = calculationsAscendingList.listOfValues(getTestList());
        Assertions.assertThat(jst.get(0).getBuyPrice()).isEqualTo(134.55);
        Assertions.assertThat(jst.get(0).getSellPrice()).isEqualTo(135.67);
        Assertions.assertThat(jst.size()==8);
        Assertions.assertThat(jst.size()-1).isNotNull();



    }
    @Test
    public void maxValueTest() throws Exception{
        CalculationsAscendingList calculationsAscendingList = new CalculationsAscendingList();
        List<JsonTestData> jst = calculationsAscendingList.listOfValues(getTestList());
        Assertions.assertThat(jst.get(jst.size()-1).getBuyPrice()).isEqualTo(177.88);
        Assertions.assertThat(jst.get(jst.size()-1).getSellPrice()).isEqualTo(178.99);
        Assertions.assertThat(jst.size()==8);
        Assertions.assertThat(jst.size()-1).isNotNull();
    }


    public List<JsonTestData> getTestList() throws ParseException {
        List<JsonTestData> testList = new ArrayList<JsonTestData>();
        String data1 = "2016-04-04";
        String data2 = "2016-04-05";
        String data3 = "2016-04-06";
        String data4 = "2016-04-07";
        String data5 = "2016-04-08";
        String data6 = "2016-04-09";
        String data7 = "2016-04-10";
        String data8 = "2016-04-11";
        String data9 = "2016-04-12";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        Date d1 = sdf.parse(data1);
        Date d2 = sdf.parse(data2);
        Date d3 = sdf.parse(data3);
        Date d4 = sdf.parse(data4);
        Date d5 = sdf.parse(data5);
        Date d6 = sdf.parse(data6);
        Date d7 = sdf.parse(data7);
        Date d8 = sdf.parse(data8);
        Date d9 = sdf.parse(data9);
        //
        testList.add(new JsonTestData(d1,134.55, 135.67));
        testList.add(new JsonTestData(d2, 177.88, 178.99));
        testList.add(new JsonTestData(d3, 154.88, 156.99));
        testList.add(new JsonTestData(d4, 155.88, 156.99));
        testList.add(new JsonTestData(d5, 156.88, 157.99));
        testList.add(new JsonTestData(d6, 157.88, 158.99));
        testList.add(new JsonTestData(d7, 158.88, 159.99));
        testList.add(new JsonTestData(d8, 159.88, 162.99));
        testList.add(new JsonTestData(d9, 177.88, 179.00));
        return testList;
    }

}