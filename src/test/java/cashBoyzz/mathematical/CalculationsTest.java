package cashBoyzz.mathematical;

import org.junit.Test;
import org.springframework.util.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class CalculationsTest {

    @Test
    public void minValueTest() throws Exception {
        Calculations calculations = new Calculations();

        Optional<JsonTestData> maxBuyPriceObject = calculations.maxBuyPriceObject(getTestList());

        Optional<JsonTestData> minSellPriceObject = calculations.minBuyPriceObject(getTestList());

//        System.out.println("Najwyższa cena skupu przez bank to: " +
//                + maxBuyPriceObject.get().getBuyPrice() + " PLN " + " w dniu: " + maxBuyPriceObject.get().getDate().toString());

        Assert.isTrue(maxBuyPriceObject.get().getDate().toString().equals("Tue Jan 05 00:04:00 CET 2016"));
        Assert.isTrue(maxBuyPriceObject.get().getBuyPrice() == 177.88);
        Assert.isTrue(minSellPriceObject.get().getDate().toString().toString().equals("Mon Jan 04 00:04:00 CET 2016"));
        Assert.isTrue(minSellPriceObject.get().getSellPrice() == 135.67);

//        System.out.println("Najniższa cena spredaży przez bank to: "
//                + minSellPriceObject.get().getSellPrice() + "PLN" + " w dniu: " + minSellPriceObject.get().getDate().toString());
    }
    public List<JsonTestData> getTestList() throws ParseException {
        List<JsonTestData> testList = new ArrayList<JsonTestData>();
        String data1 = "2016-04-04";
        String data2 = "2016-04-05";
        String data3 = "2016-04-06";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        Date d1 = sdf.parse(data1);
        Date d2 = sdf.parse(data2);
        Date d3 = sdf.parse(data3);
        //
        testList.add(new JsonTestData(d1,134.55, 135.67));
        testList.add(new JsonTestData(d2, 177.88, 178.99));
        testList.add(new JsonTestData(d3, 154.88, 156.99));
        return testList;
    }
}