package cashBoyzz.DataSource;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class GoldNbpDataProviderTest {

    @Test
    public void readJsonFromUrlForGoldLastFewDays() throws Exception {
        GoldNbpDataProvider jsonReader = new GoldNbpDataProvider();
        List<NbpRateGoldLastDays> jsonObject1 = jsonReader.readJsonFromUrlForGoldLastFewDays(01);
        Assertions.assertThat(jsonObject1.size()).isBetween(4,8);
        Assertions.assertThat(jsonObject1.size()).isNotZero();
    }

}