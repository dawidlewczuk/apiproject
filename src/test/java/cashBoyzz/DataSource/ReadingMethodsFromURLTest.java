package cashBoyzz.DataSource;

import org.junit.Test;
import org.springframework.util.Assert;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.*;

public class ReadingMethodsFromURLTest {
    @Test
    public void getFromURL() throws Exception {

        String urlTest = "http://api.nbp.pl/api/exchangerates/rates/c/usd/2016-04-04/?format=json";
        InputStream is = new URL(urlTest).openStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
        Assert.notNull(reader,"{\n" +
                "  \"table\": \"C\",\n" +
                "  \"currency\": \"dolar amerykański\",\n" +
                "  \"code\": \"USD\",\n" +
                "  \"rates\": [\n" +
                "    {\n" +
                "      \"no\": \"064/C/NBP/2016\",\n" +
                "      \"effectiveDate\": \"2016-04-04\",\n" +
                "      \"bid\": 3.6929,\n" +
                "      \"ask\": 3.7675\n" +
                "    }\n" +
                "  ]\n" +
                "}");
        Assert.notNull(is,"http://api.nbp.pl/api/exchangerates/rates/c/usd/2016-04-04/?format=json");

    }

}