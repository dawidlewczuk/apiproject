package cashBoyzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "cashBoyzz")

public class Aplication {
    public static void main(final String[] args) {
        SpringApplication.run(Aplication.class, args);
    }}

//run http://localhost:8084/swagger-ui.html

