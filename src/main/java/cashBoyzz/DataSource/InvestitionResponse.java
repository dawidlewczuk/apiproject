package cashBoyzz.DataSource;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import java.util.Date;

public class InvestitionResponse {
    //highest price you can get for selling your currency
    private double maxBuyPrice;
    //lowest price you have to pay for the currency in given time period
    private double minPriceSell;

    //date, when you can get the highest price for selling your currency
    @JsonFormat(pattern="yyyy-MM-dd")
    private  Date dateForMaxBuyPrice;
    //date, when you pay the lowest price of the currency for given time period
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateForMinSellPrice;

    //potencial income(assuming that currency did not loose the value
    private BigDecimal investementReturn;

    public InvestitionResponse(double maxBuyPrice, double minPriceSell, Date dateForMaxBuyPrice, Date dateForMinSellPrice, BigDecimal investementReturn) {
        this.maxBuyPrice = maxBuyPrice;
        this.minPriceSell = minPriceSell;
        this.dateForMaxBuyPrice = dateForMaxBuyPrice;
        this.dateForMinSellPrice = dateForMinSellPrice;
        this.investementReturn = investementReturn;
    }

    public double getMaxBuyPrice() {
        return maxBuyPrice;
    }

    public double getMinPriceSell() {
        return minPriceSell;
    }

    public Date getDateForMaxBuyPrice() {
        return dateForMaxBuyPrice;
    }

    public Date getDateForMinSellPrice() {
        return dateForMinSellPrice;
    }

    public BigDecimal getInvestementReturn() {
        return investementReturn;
    }
}
