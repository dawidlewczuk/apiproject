package cashBoyzz.DataSource;

import java.math.BigDecimal;

public class InvestitionResponseGold {

    private double maxBuyPriceGold;

    private double minBuyPriceGold;

    private BigDecimal investmentReturnOrLooseForGold;

    private String lowestPriceDate;

    private String highestPriceDate;

    public InvestitionResponseGold(double maxBuyPriceGold, double minBuyPriceGold, BigDecimal investmentReturnForGold, String lowestPriceDate, String highestPriceDate) {
        this.maxBuyPriceGold = maxBuyPriceGold;
        this.minBuyPriceGold = minBuyPriceGold;
        this.investmentReturnOrLooseForGold = investmentReturnForGold;
        this.lowestPriceDate = lowestPriceDate;
        this.highestPriceDate = highestPriceDate;
    }

    public double getMaxBuyPriceGold() {
        return maxBuyPriceGold;
    }

    public void setMaxBuyPriceGold(double maxBuyPriceGold) {
        this.maxBuyPriceGold = maxBuyPriceGold;
    }

    public double getMinBuyPriceGold() {
        return minBuyPriceGold;
    }

    public void setMinBuyPriceGold(double minBuyPriceGold) {
        this.minBuyPriceGold = minBuyPriceGold;
    }

    public BigDecimal getInvestmentReturnForGold() {
        return investmentReturnOrLooseForGold;
    }

    public void setInvestmentReturnForGold(BigDecimal investmentReturnForGold) {
        this.investmentReturnOrLooseForGold = investmentReturnForGold;
    }

    public String getLowestPriceDate() {
        return lowestPriceDate;
    }

    public void setLowestPriceDate(String lowestPriceDate) {
        this.lowestPriceDate = lowestPriceDate;
    }

    public String getHighestPriceDate() {
        return highestPriceDate;
    }

    public void setHighestPriceDate(String highestPriceDate) {
        this.highestPriceDate = highestPriceDate;
    }
}
