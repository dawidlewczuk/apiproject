package cashBoyzz.DataSource;

import java.util.List;


public class NbpResult {

    private String table;
    private String currency;
    private String code;

    private List<NbpRate> rates;

    public String getTable() {
        return table;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "nbpResult{" +
                "table='" + table + '\'' +
                ", currency='" + currency + '\'' +
                ", code='" + code + '\'' +
                ", rates=" + rates +
                '}';
    }

    public  List<NbpRate> getRates() {
        return rates;
    }

}
