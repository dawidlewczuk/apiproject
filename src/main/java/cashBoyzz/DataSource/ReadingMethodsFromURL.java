package cashBoyzz.DataSource;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

public class ReadingMethodsFromURL {

    static ObjectMapper mapper = new ObjectMapper();

    static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    static String getFromURL(String url) throws IOException {
        try (InputStream is = new URL(url).openStream()){
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            return readAll(reader);
        }
    }
}
