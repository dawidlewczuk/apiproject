package cashBoyzz.DataSource;

import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class GoldNbpDataProvider extends ReadingMethodsFromURL {


    public List<NbpRateGoldLastDays> readJsonFromUrlForGoldLastFewDays(Integer DayCount) throws IOException{

        String url = "http://api.nbp.pl/api/cenyzlota/last/"+DayCount+"/?format=json";
        String jsonText1 = getFromURL(url);
        List<NbpRateGoldLastDays> jsonObject1 = mapper.readValue(jsonText1, new TypeReference<List<NbpRateGoldLastDays>>(){});
        return jsonObject1;
    }

}
