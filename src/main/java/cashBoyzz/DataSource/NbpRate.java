package cashBoyzz.DataSource;

import java.util.Date;

public class NbpRate {

    private String no;
    private Date effectiveDate;
    private Double bid;
    private Double ask;

    public NbpRate(){}

    public NbpRate(Date effectiveDate, Double bid, Double ask) {
        this.effectiveDate = effectiveDate;
        this.bid = bid;
        this.ask = ask;
    }

    public String getNo() {
        return no;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public Double getBid() {
        return bid;
    }

    public Double getAsk() {
        return ask;
    }


}
