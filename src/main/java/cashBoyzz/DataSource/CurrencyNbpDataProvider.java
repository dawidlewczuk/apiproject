package cashBoyzz.DataSource;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Scanner;

@Component
public class CurrencyNbpDataProvider extends ReadingMethodsFromURL {

    public NbpResult readJsonFromUrl(String data1, String data2, Currency currency) throws IOException{
        String url = "http://api.nbp.pl/api/exchangerates/rates/c/" + currency.toString().toLowerCase() + "/" + data1 + "/" + data2 + "/?format=json";
        String jsonText = getFromURL(url);
        NbpResult jsonObject = mapper.readValue(jsonText, NbpResult.class);
        return jsonObject;
    }
}