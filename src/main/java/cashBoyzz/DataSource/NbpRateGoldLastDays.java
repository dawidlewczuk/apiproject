package cashBoyzz.DataSource;

import java.util.List;

public class NbpRateGoldLastDays {

    private String data;
    private Double cena;

    private List<NbpRateGoldLastDays> rates1;

    public List<NbpRateGoldLastDays> getRates1() {
        return rates1;
    }

    public NbpRateGoldLastDays() {
    }

    public NbpRateGoldLastDays(String data, Double cena) {
        this.data = data;
        this.cena = cena;
    }

    public String getData() {
        return data;
    }

    public Double getCena() {
        return cena;
    }

}
