package cashBoyzz.mathematical;

import java.util.*;

//this class is made simple to create calculation for this task


public class JsonTestData {

    private Date date;
    private double buyPrice;
    private double sellPrice;

    public JsonTestData(Date date, double buyPrice, double sellPrice) {
        this.date = date;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
    }

    public Date getDate() {
        return date;
    }


    public double getBuyPrice() {
        return buyPrice;
    }


    public double getSellPrice() {
        return sellPrice;
    }
}
