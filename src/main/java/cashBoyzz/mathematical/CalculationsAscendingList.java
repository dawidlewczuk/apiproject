package cashBoyzz.mathematical;


import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class CalculationsAscendingList {

    public CalculationsAscendingList() {
        super();
    }

    public List<JsonTestData> listOfValues(List<JsonTestData> data){
        List<JsonTestData> longest = new ArrayList<JsonTestData>();
        List<JsonTestData> current = new ArrayList<JsonTestData>();
        double previous = Double.MAX_VALUE;
        for (JsonTestData value : data) {
            if (value.getBuyPrice() <= previous) {
                if (longest.size() < current.size()) {
                    longest = current;
                }
                current = new ArrayList<JsonTestData>();
            }
            current.add(value);
            previous = value.getBuyPrice();
        }
        return (longest.get(longest.size()-1).getBuyPrice() - longest.get(0).getBuyPrice()) < (current.get(current.size()-1).getBuyPrice() - current.get(0).getBuyPrice()) ? current : longest;
    }

    }



