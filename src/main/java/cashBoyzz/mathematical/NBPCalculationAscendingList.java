package cashBoyzz.mathematical;

import cashBoyzz.DataSource.Currency;
import cashBoyzz.DataSource.CurrencyNbpDataProvider;
import cashBoyzz.DataSource.NbpResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class NBPCalculationAscendingList {

    public NBPCalculationAscendingList() {
    }

    @Autowired
    CurrencyNbpDataProvider nbpDataProvider;
    @Autowired
    CalculationsAscendingList calculationsAscendingList;
    @Autowired
    NBPtoCalculationMapper mapper;

    public JsonTestData min(Date startDate, Date endDate, Currency currency){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            String dateStart = sdf.format(startDate);
            String dateEnd = sdf.format(endDate);
            NbpResult nbpResult = nbpDataProvider.readJsonFromUrl(dateStart, dateEnd, currency);
            List<JsonTestData> values = calculationsAscendingList.listOfValues(mapper.map(nbpResult.getRates()));
            return values.get(0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonTestData max(Date startDate, Date endDate, Currency currency){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            String dateStart = sdf.format(startDate);
            String dateEnd = sdf.format(endDate);
            NbpResult nbpResult = nbpDataProvider.readJsonFromUrl(dateStart, dateEnd, currency);
            List<JsonTestData> values = calculationsAscendingList.listOfValues(mapper.map(nbpResult.getRates()));
            return values.get(values.size()-1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
