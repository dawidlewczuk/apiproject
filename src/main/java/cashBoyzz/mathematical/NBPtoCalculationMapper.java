package cashBoyzz.mathematical;


import cashBoyzz.DataSource.NbpRate;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class NBPtoCalculationMapper {

    public List<JsonTestData> map(List<NbpRate> nbpRate) {
        List<JsonTestData> result = new ArrayList<>();

        for (cashBoyzz.DataSource.NbpRate rate : nbpRate) {
            JsonTestData data = new JsonTestData(rate.getEffectiveDate(),rate.getBid(),rate.getAsk());
            result.add(data);
        }
        return result;
    }
}

