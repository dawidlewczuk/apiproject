package cashBoyzz.mathematical;

import org.springframework.stereotype.Service;
import java.math.BigDecimal;

@Service
public class NBPIncomeCalculate {
    public NBPIncomeCalculate() {
    }

    public BigDecimal getPotencialIncomeResult(Double maxBuy, Double minSell, Double investment){
        BigDecimal investmentReturn = new BigDecimal((maxBuy - minSell) * investment);
        return investmentReturn.setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}
