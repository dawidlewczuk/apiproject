package cashBoyzz.mathematical;

import cashBoyzz.DataSource.Currency;
import cashBoyzz.DataSource.CurrencyNbpDataProvider;
import cashBoyzz.DataSource.NbpResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Service
public class NBPCalculations {

    public NBPCalculations() {
        super();
    }

    @Autowired
    CurrencyNbpDataProvider nbpDataProvider;
    @Autowired
    Calculations calculations;
    @Autowired
    NBPtoCalculationMapper mapper;

    public JsonTestData min(Date startDate, Date endDate, Currency currency) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            String dateStart = sdf.format(startDate);
            String dateEnd = sdf.format(endDate);
            NbpResult nbpResult = nbpDataProvider.readJsonFromUrl(dateStart, dateEnd, currency);
            return calculations.minBuyPriceObject(mapper.map(nbpResult.getRates())).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public JsonTestData max(Date startDate, Date endDate, Currency currency){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            String dateStart = sdf.format(startDate);
            String dateEnd = sdf.format(endDate);

            NbpResult nbpResult = nbpDataProvider.readJsonFromUrl(dateStart, dateEnd, currency);
            return calculations.maxBuyPriceObject(mapper.map(nbpResult.getRates())).get();

        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }
}
