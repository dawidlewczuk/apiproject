package cashBoyzz.mathematical;

import cashBoyzz.DataSource.GoldNbpDataProvider;
import cashBoyzz.DataSource.NbpRateGoldLastDays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class NBPCalculationGold {
    public NBPCalculationGold() {
    }

    @Autowired
    CalculationGold calculationGold;

    @Autowired
    GoldNbpDataProvider goldNbpDataProvider;

    public NbpRateGoldLastDays minGold(Integer dayCount){
        try {
            List<NbpRateGoldLastDays> nbpRateGoldLastDays = goldNbpDataProvider.readJsonFromUrlForGoldLastFewDays(dayCount);
            return calculationGold.GoldPriceMin(nbpRateGoldLastDays).get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public NbpRateGoldLastDays maxGold(Integer dayCount){
        try {
            List<NbpRateGoldLastDays> nbpRateGoldLastDays = goldNbpDataProvider.readJsonFromUrlForGoldLastFewDays(dayCount);
            return calculationGold.GoldPriceMax(nbpRateGoldLastDays).get();
        } catch (IOException eo){
            throw new RuntimeException(eo);
        }
    }
}
