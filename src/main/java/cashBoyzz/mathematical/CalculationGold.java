package cashBoyzz.mathematical;

import cashBoyzz.DataSource.NbpRateGoldLastDays;
import org.springframework.stereotype.Component;

import javax.swing.text.html.Option;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Component
public class CalculationGold {

    public CalculationGold() {
    }

    public Optional<NbpRateGoldLastDays> GoldPriceMax (List<NbpRateGoldLastDays> dataGold){
        Optional<NbpRateGoldLastDays> GoldPriceMax = dataGold.stream().max(Comparator.comparingDouble(NbpRateGoldLastDays::getCena));
    return GoldPriceMax;
    }

    public Optional<NbpRateGoldLastDays> GoldPriceMin (List<NbpRateGoldLastDays> dataGold){
        Optional<NbpRateGoldLastDays> GoldPriceMin = dataGold.stream().min(Comparator.comparingDouble(NbpRateGoldLastDays::getCena));
        return GoldPriceMin;
    }
}
