package cashBoyzz.mathematical;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;

@Component
public class Calculations {

    public Calculations() throws ParseException {
    }

    public Optional<JsonTestData> maxBuyPriceObject(List<JsonTestData> data){
        Optional<JsonTestData> maxBuyPriceObject = data.stream().max(Comparator.comparingDouble(JsonTestData::getBuyPrice));
        return maxBuyPriceObject;
    }

    public Optional<JsonTestData> minBuyPriceObject(List<JsonTestData> data){
        Optional<JsonTestData> minSellPriceObject = data.stream().min(Comparator.comparingDouble(JsonTestData::getSellPrice));
        return minSellPriceObject;
    }

}
