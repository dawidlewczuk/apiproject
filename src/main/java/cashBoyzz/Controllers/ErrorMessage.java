package cashBoyzz.Controllers;

public class ErrorMessage {
    private String url;
    private int status;
    private String message;

    public ErrorMessage(String url, int status, String message) {
        this.url = url;
        this.status = status;
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
