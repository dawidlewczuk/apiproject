package cashBoyzz.Controllers;

import cashBoyzz.DataSource.Currency;
import cashBoyzz.DataSource.InvestitionResponse;
import cashBoyzz.DataSource.InvestitionResponseGold;
import cashBoyzz.DataSource.NbpRateGoldLastDays;
import cashBoyzz.mathematical.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;


@RestController
public class NBPDataProviderControler {

    @Autowired
    private NBPCalculations nbpCalculations;

    @Autowired
    private NBPIncomeCalculate nbpIncomeCalculate;

    @Autowired
    private NBPCalculationAscendingList ascendingList;

    @Autowired
    private CalculationGold calculationGold;

    @Autowired
    private NBPCalculationGold nbpCalculationGold;


    @ExceptionHandler(NoDataFoundException.class)
    public ErrorMessage noDataAvailable(final HttpServletRequest request,
                                        final HttpServletResponse response, final Exception exception) {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return new ErrorMessage(request.getRequestURL().toString(), response.getStatus(),
                exception.getMessage());
    }

    @RequestMapping(value = "/NBPCurrencyCalculations", method = RequestMethod.GET)
    public ResponseEntity<InvestitionResponse> getNbpResult(
            @RequestParam(name = "currency") final Currency currency,
            @RequestParam(name = "startDate in format: yyyy-mm-dd") @DateTimeFormat(pattern = "yyyy-mm-dd") final Date startDate,
            @RequestParam(name = "endDate in format: yyyy-mm-dd") @DateTimeFormat(pattern = "yyyy-mm-dd") final Date endDate,
            @RequestParam(name = "investment in chosen currency") Double investment)
            throws NoDataFoundException, NotFoundException {
        if (startDate != null && endDate != null) {
            JsonTestData NBPobjectmax = nbpCalculations.max(startDate, endDate, currency);
            JsonTestData NBPobjectmin = nbpCalculations.min(startDate, endDate, currency);

            double maxBuy = NBPobjectmax.getBuyPrice();
            double minSell = NBPobjectmin.getSellPrice();
            Date dateMinSellPrice = NBPobjectmin.getDate();
            Date dateMaxBuyPrice = NBPobjectmax.getDate();
            BigDecimal investmentReturnRounded = nbpIncomeCalculate.getPotencialIncomeResult(maxBuy, minSell, investment);

            //below method finds the biggest ascending grow in whole data
            if (dateMinSellPrice.after(dateMaxBuyPrice)) {
                JsonTestData ascendingObjectMax = ascendingList.max(startDate, endDate, currency);
                JsonTestData ascendingObjectMin = ascendingList.min(startDate, endDate, currency);

                double maxBuyAscending = ascendingObjectMax.getSellPrice();
                double minSellAscending = ascendingObjectMin.getBuyPrice();
                Date dateMinSellPriceAscending = ascendingObjectMin.getDate();
                Date dateMaxBuyPriceAscending = ascendingObjectMax.getDate();
                BigDecimal investmentReturnRoundedAscending = nbpIncomeCalculate.getPotencialIncomeResult(maxBuyAscending, minSellAscending, investment);
                return new ResponseEntity<InvestitionResponse>(new InvestitionResponse(maxBuyAscending, minSellAscending, dateMaxBuyPriceAscending, dateMinSellPriceAscending, investmentReturnRoundedAscending)
                        , HttpStatus.OK);
            } else {
                return new ResponseEntity<InvestitionResponse>(new InvestitionResponse(maxBuy, minSell, dateMaxBuyPrice, dateMinSellPrice, investmentReturnRounded)
                        , HttpStatus.OK);
            }
        }
        return null;
    }

    @RequestMapping(value = "/NBP-GoldCalculations", method = RequestMethod.GET)
    public ResponseEntity<InvestitionResponseGold> getNbpGoldResult(
            @RequestParam(name = "dayCount") final Integer DayCount,
            @RequestParam(name = "investment in 1g of Gold") final Double investment)
            throws NoDataFoundException, NotFoundException {
        if (DayCount != null) {
            NbpRateGoldLastDays goldObjectMax = nbpCalculationGold.maxGold(DayCount);
            NbpRateGoldLastDays goldObjectMin = nbpCalculationGold.minGold(DayCount);
            double maxBuyPriceGold = goldObjectMax.getCena();
            double minBuyPriceGold = goldObjectMin.getCena();

            String maxBuyDate = goldObjectMax.getData();
            String minBuyDate = goldObjectMin.getData();
            BigDecimal investmentReturn = new BigDecimal((maxBuyPriceGold - minBuyPriceGold) * investment).setScale(2, BigDecimal.ROUND_HALF_UP);

            return new ResponseEntity<InvestitionResponseGold>(new InvestitionResponseGold(maxBuyPriceGold, minBuyPriceGold, investmentReturn, minBuyDate, maxBuyDate), HttpStatus.OK);
        }
        return null;
    }
}

