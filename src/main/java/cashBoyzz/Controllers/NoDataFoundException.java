package cashBoyzz.Controllers;

public class NoDataFoundException extends Exception{

    public NoDataFoundException(final String message){
        super(message);
    }
}
